package jp.alhinc.kobayashi_masafumi.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CalculateSales {

	public static void main(String[] args) {

		Map<String, String> branchNames = new HashMap<>();
		Map<String, Long> branchSales = new HashMap<>();

		if (args.length != 1) {
			System.out.println("予期せぬエラーが発生しました");
			return;
		}

		if (!input(args[0], "branch.lst", "^[0-9]{3}", "支店定義", branchNames, branchSales)) {
			return;
		}

		// 売上ファイルの判定
		File[] files = new File(args[0]).listFiles();
		List<File> rcdFiles = new ArrayList<>();

		for (int i = 0; i < files.length; i++) {
			String rcd = files[i].getName();
			if (files[i].isFile() && rcd.matches("[0-9]{8}.+rcd$")) {
				rcdFiles.add(files[i]);
			}
		}

		Collections.sort(rcdFiles);

		// 売上ファイル連番エラー処理
		for (int i = 0; i < rcdFiles.size() - 1; i++) {
			String fileName = (rcdFiles.get(i).getName());
			String nextFileName = (rcdFiles.get(i + 1).getName());
			int formerFile = Integer.parseInt(fileName.substring(0, 8));
			int latterFile = Integer.parseInt(nextFileName.substring(0, 8));

			if ((latterFile - formerFile) != 1) {
				System.out.println("売上ファイル名が連番になっていません");
				return;
			}
		}

		// 売上ファイルの中身を抽出
		for (File fileSale : rcdFiles) {
			BufferedReader br = null;
			try {
				FileReader fr = new FileReader(fileSale);
				br = new BufferedReader(fr);
				String line;
				List<String> sales = new ArrayList<>();
				while ((line = br.readLine()) != null) {
					sales.add(line);
				}

				if (sales.size() != 2) {
					System.out.println(fileSale.getName() + "のフォーマットが不正です");
					return;
				}

				if (!sales.get(1).matches("^[0-9]*$")) {
					System.out.println("予期せぬエラーが発生しました");
					return;
				}

				if (!branchNames.containsKey(sales.get(0))) {
					System.out.println(fileSale.getName() + "の支店コードが不正です");
					return;
				}

				long salesAmount = Long.parseLong(sales.get(1));
				long total = branchSales.get(sales.get(0)) + salesAmount;

				if (total >= 10000000000L) {
					System.out.println("合計金額が10桁を超えました");
					return;
				}

				branchSales.put(sales.get(0), total);

			} catch (IOException e) {
				System.out.println("予期せぬエラーが発生しました");
				return;
			} finally {
				if (br != null) {
					try {
						br.close();
					} catch (IOException e) {
						System.out.println("予期せぬエラーが発生しました");
						return;
					}
				}
			}
		}

		if (!output(args[0], "branch.out", branchNames, branchSales)) {
			return;
		}
	}

	private static boolean input(String wayPath, String filePlace, String Terms, String Office, Map<String, String> branchNames, Map<String, Long> branchSales) {
		// ファイル読み込みでの例外処理
		BufferedReader br = null;
		try {
			File file = new File(wayPath, filePlace);

			// 支店定義ファイルの存在確認
			if (!file.exists()) {
				System.out.println( Office + "ファイルが存在しません");
				return false;
			}
			FileReader fr = new FileReader(file);
			br = new BufferedReader(fr);
			String line;
			while ((line = br.readLine()) != null) {
				String[] items = line.split(",");

				// 支店定義ファイルのフォーマット確認
				if ((items.length != 2) || (!items[0].matches(Terms))) {
					System.out.println( Office + "ファイルのフォーマットが不正です");
					return false;
				}
				branchNames.put(items[0], items[1]);
				branchSales.put(items[0], 0L);
			}
		} catch (IOException e) {
			System.out.println("予期せぬエラーが発生しました");
			return false;
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					System.out.println("予期せぬエラーが発生しました");
					return false;
				}
			}
		}
		return true;
	}

	private static boolean output(String dirpath, String dirFile, Map<String, String> branchNames,
			Map<String, Long> branchSales) {

		// 支店別集計ファイルの出力
		BufferedWriter bw = null;
		try {
			File file = new File(dirpath, dirFile);
			FileWriter fw = new FileWriter(file);
			bw = new BufferedWriter(fw);

			for (String key : branchSales.keySet()) {
				bw.write(key + "," + branchNames.get(key) + "," + branchSales.get(key));
				bw.newLine();
			}
		} catch (IOException e) {
			System.out.println("予期せぬエラーが発生しました");
			return false;
		} finally {
			if (bw != null) {
				try {
					bw.close();
				} catch (IOException e) {
					System.out.println("予期せぬエラーが発生しました");
					return false;
				}
			}
		}
		return true;
	}
}
